Pod::Spec.new do |s|
  s.name             = 'FSCore'
  s.version          = '1.0.9'
  s.summary          = 'Fox Sports Core library'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Melad Barjel' => 'melad.barjel@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2018 Fox Sports' }

  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/e3bfa83f78737400559b01d6b2a89d6bf94509d8/Archives/FSCore/FSCore-1.0.9.zip' }
  s.ios.vendored_frameworks = 'FSCore.framework'

  s.platform                = :ios
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  s.tvos.deployment_target  = '10.0'

  s.dependency 'RxCocoa', '4.4.1'
  s.dependency 'RxSwift', '4.4.1'
  s.dependency 'AlamofireImage', '3.4.1'
  s.dependency 'Alamofire', '4.7.3'
  
end
