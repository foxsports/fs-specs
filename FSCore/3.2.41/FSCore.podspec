Pod::Spec.new do |s|
  s.name             = 'FSCore'
  s.version          = '3.2.41'
  s.summary          = 'Fox Sports Core library'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Melad Barjel' => 'melad.barjel@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2018 Fox Sports' }

  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/f6325800248e5d0518a150760ec35a93114ff491/Archives/FSCore/FSCore-3.2.41.zip' }
  s.ios.vendored_frameworks = 'FSCore.framework'

  s.platform                = :ios
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '10.0'
  s.tvos.deployment_target  = '10.0'

  s.dependency 'RxCocoa', '~> 5.0'
  s.dependency 'RxSwift', '~> 5.0'
  s.dependency 'SDWebImage', '~> 5.0'
  
end
