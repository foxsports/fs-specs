Pod::Spec.new do |s|
  s.name             = 'FSCore'
  s.version          = '1.0.7'
  s.summary          = 'Fox Sports Core library'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Melad Barjel' => 'melad.barjel@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2018 Fox Sports' }

  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/ee99488717f180f9fdd876e0d8a99c8c541b4e6a/Archives/FSCore/FSCore-1.0.7.zip' }
  s.ios.vendored_frameworks = 'FSCore.framework'

  s.platform                = :ios
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  s.tvos.deployment_target  = '10.0'

  s.dependency 'RxCocoa', '~> 4.0'
  s.dependency 'RxSwift', '~> 4.0'
  s.dependency 'AlamofireImage'
  
end
