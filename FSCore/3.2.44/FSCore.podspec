Pod::Spec.new do |s|
  s.name             = 'FSCore'
  s.version          = '3.2.44'
  s.summary          = 'Fox Sports Core library'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Melad Barjel' => 'melad.barjel@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2022 Fox Sports' }

  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/fd7434ecf33c987f2d993bd6d4e75469ea574df0/Archives/FSCore/FSCore-3.2.44.zip' }
  s.ios.vendored_frameworks = 'FSCore.framework'

  s.platform                = :ios
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '12.0'
  s.tvos.deployment_target  = '12.0'

  s.dependency 'RxCocoa', '~> 5.0'
  s.dependency 'RxSwift', '~> 5.0'
  s.dependency 'SDWebImage', '~> 5.0'
  
end
