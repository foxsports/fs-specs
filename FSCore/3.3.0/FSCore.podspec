Pod::Spec.new do |s|
  s.name             = 'FSCore'
  s.version          = '3.3.0'
  s.summary          = 'Fox Sports Core library'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { "Alessandro Boron" => "alessandro.boron@foxtel.com.au" }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2022 Fox Sports' }

  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/2197ede1970df59d9267d34d3f90d3f5b1a5f3c5/Archives/FSCore/FSCore-3.3.0.zip' }
  s.ios.vendored_frameworks = 'FSCore.xcframework'

  s.platform                = :ios
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '13.0'
  s.tvos.deployment_target  = '13.0'

  s.dependency 'RxCocoa', '~> 5.0'
  s.dependency 'RxSwift', '~> 5.0'
  s.dependency 'SDWebImage', '~> 5.0'
  
end
