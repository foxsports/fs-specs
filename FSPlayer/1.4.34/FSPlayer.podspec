Pod::Spec.new do |s|

  s.name         = "FSPlayer"
  s.version      = "1.4.34"
  s.summary      = "Fox Sports Video Player"
  s.homepage     = "http://foxsports.com.au"
  s.author       = { "Melad Barjel" => "melad.barjel@foxsports.com.au" }
  s.license      = { :type => 'Copyright', :text => 'Copyright 2022 Fox Sports' }

  s.source                   = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/af28b4a81f9e1a04ca07c0f1323ab48a52939fed/Archives/FSPlayer/FSPlayer-1.4.34.zip' }
  s.ios.vendored_frameworks  = 'FSPlayer.framework'
  
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '12.0'
  s.tvos.deployment_target  = '12.0'
  
  s.framework               = 'AVFoundation'

  s.dependency 'FSCore', '3.2.44'
  s.dependency 'PHDiff', '~> 1.1'
  s.dependency 'SnapKit', '~> 5.0'
  s.dependency 'NSObject+Rx', '~> 5.0'
  
end
