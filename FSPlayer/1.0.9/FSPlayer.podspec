Pod::Spec.new do |s|

  s.name         = "FSPlayer"
  s.version      = "1.0.9"
  s.summary      = "Fox Sports Video Player"
  s.homepage     = "http://foxsports.com.au"
  s.author       = { "Melad Barjel" => "melad.barjel@foxsports.com.au" }
  s.license      = { :type => 'Copyright', :text => 'Copyright 2018 Fox Sports' }

  s.source                   = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/e3bfa83f78737400559b01d6b2a89d6bf94509d8/Archives/FSPlayer/FSPlayer-1.0.9.zip' }
  s.ios.vendored_frameworks  = 'FSPlayer_iOS.framework'
  
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  s.tvos.deployment_target  = '10.0'
  
  s.framework               = 'AVFoundation'

  s.dependency 'FSCore', '1.0.9'
  s.dependency 'SnapKit', '~> 4.0'
  s.dependency 'NSObject+Rx', '4.4.1'
  s.dependency 'RxDataSources', '3.1.0'
  s.dependency 'PHDiff', '~> 1.1'
  
end
