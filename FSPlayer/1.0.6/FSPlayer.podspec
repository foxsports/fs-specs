Pod::Spec.new do |s|

  s.name         = "FSPlayer"
  s.version      = "1.0.6"
  s.summary      = "Fox Sports Video Player"
  s.homepage     = "http://foxsports.com.au"
  s.author       = { "Melad Barjel" => "melad.barjel@foxsports.com.au" }
  s.license      = { :type => 'Copyright', :text => 'Copyright 2018 Fox Sports' }

  s.source                   = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/260f126d6c8d097f7224ccc27a8df9ef18c91aaf/Archives/FSPlayer/FSPlayer-1.0.6.zip' }
  s.ios.vendored_frameworks  = 'FSPlayer_iOS.framework'
  
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  s.tvos.deployment_target  = '10.0'
  
  s.framework               = 'AVFoundation'

  s.dependency 'FSCore', '1.0.5'
  s.dependency 'SnapKit', '~> 4.0'
  s.dependency 'NSObject+Rx', '~> 4.0'
  s.dependency 'RxDataSources', '~> 3.0'
  
end
