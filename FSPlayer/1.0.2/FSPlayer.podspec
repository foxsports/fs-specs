Pod::Spec.new do |s|

  s.name         = "FSPlayer"
  s.version      = "1.0.2"
  s.summary      = "Fox Sports Video Player"
  s.homepage     = "http://foxsports.com.au"
  s.author       = { "Melad Barjel" => "melad.barjel@foxsports.com.au" }
  s.license      = { :type => 'Copyright', :text => 'Copyright 2018 Fox Sports' }

  s.source                   = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/65d282d8e91bed6f4ba47addcf5d3c4630974495/FSPlayer.zip' }
  s.ios.vendored_frameworks  = 'FSPlayer_iOS.framework'
  
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  s.tvos.deployment_target  = '10.0'
  
  s.framework               = 'AVFoundation'

  s.dependency 'FSCore', '1.0.2'
  s.dependency 'SnapKit', '~> 4.0'
  s.dependency 'NSObject+Rx', '~> 4.0'
  s.dependency 'RxDataSources', '~> 3.0'

  s.ios.dependency 'google-cast-sdk', '~> 4.0.2'

  
end
