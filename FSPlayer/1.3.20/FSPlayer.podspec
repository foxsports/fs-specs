Pod::Spec.new do |s|

  s.name         = "FSPlayer"
  s.version      = "1.3.20"
  s.summary      = "Fox Sports Video Player"
  s.homepage     = "http://foxsports.com.au"
  s.author       = { "Melad Barjel" => "melad.barjel@foxsports.com.au" }
  s.license      = { :type => 'Copyright', :text => 'Copyright 2019 Fox Sports' }

  s.source                   = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/69c46591f16679bc8c85fecb9167206fa5108f70/Archives/FSPlayer/FSPlayer-1.3.20.zip' }
  s.ios.vendored_frameworks  = 'FSPlayer.framework'
  
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  s.tvos.deployment_target  = '10.0'
  
  s.framework               = 'AVFoundation'

  s.dependency 'FSCore', '3.2.18'
  s.dependency 'PHDiff', '~> 1.1'
  s.dependency 'SnapKit', '~> 5.0'
  s.dependency 'NSObject+Rx', '~> 5.0'
  
end
