Pod::Spec.new do |s|

  s.name         = "FSPlayerKit"
  s.version      = "1.7.0"
  s.summary      = "Fox Sports Video Player"
  s.homepage     = "http://foxsports.com.au"
  s.author       = { "Alessandro Boron" => "alessandro.boron@foxtel.com.au" }
  s.license      = { :type => 'Copyright', :text => 'Copyright 2022 Fox Sports' }

  s.source                   = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/c0126ad1b37f826d6f682af1adfe394a95cda11f/Archives/FSPlayerKit/FSPlayerKit-1.7.0.zip' }
  s.ios.vendored_frameworks  = 'FSPlayerKit.xcframework'
  
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '13.0'
  s.tvos.deployment_target  = '13.0'
  
  s.framework               = 'AVFoundation'

  s.dependency 'FSCore', '3.3.0'
  s.dependency 'SnapKit', '~> 5.0'
  s.dependency 'NSObject+Rx', '~> 5.0'
  
end
