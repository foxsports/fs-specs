Pod::Spec.new do |s|
  s.name             = 'FSVideoKit'
  s.version          = '1.0.2'
  s.summary          = 'Fox Sports Video Kit'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Melad Barjel' => 'Melad.Barjel@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2022 Fox Sports' }
  
  s.source           = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/78f48e300fc53c075f6394b466f73ced4e7a8824/Archives/FSVideoKit/FSVideoKit-1.0.2.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.framework'
  
  s.platform                = :ios
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '12.0'
  
  # internal dependencies
  s.dependency 'FSCore', '3.2.44'
  s.dependency 'FSPlayer', '1.4.34'

  # external dependencies
  s.dependency 'Auth0', '1.33.1'
  s.dependency 'YouboraAVPlayerAdapter', '6.6.6'
  s.dependency 'JWTDecode', '2.6.1'
  s.dependency 'Alamofire', '~> 5.4'
  
end
