Pod::Spec.new do |s|
  s.name             = 'FSVideoKit'
  s.version          = '1.2.1'
  s.summary          = 'Fox Sports Video Kit'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Alessandro Boron' => 'Alessandro.Boron@streamotion.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2022 Fox Sports' }
  
  s.source           = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/af17eea5c234a7b1b166bf7a677c483d94378eb1/Archives/FSVideoKit/FSVideoKit-1.2.1.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.xcframework'
  
  s.platform                = :ios
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '13.0'
  
  # internal dependencies
  s.dependency 'FSCore', '3.3.0'
  s.dependency 'FSPlayerKit', '1.7.0'

  # external dependencies
  s.dependency 'Auth0', '1.39.0'
  s.dependency 'SimpleKeychain', '0.12.5'
  s.dependency 'YouboraAVPlayerAdapter', '6.6.6'
  s.dependency 'YouboraLib', '6.6.20'
  s.dependency 'JWTDecode', '2.6.1'
  s.dependency 'Alamofire', '~> 5.4'
  
end
