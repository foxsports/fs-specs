Pod::Spec.new do |s|

  s.name             = 'FSVideoKit'
  s.version          = '0.2.3'
  s.summary          = 'Fox Sports Video Kit'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Imran Raheem' => 'imran.raheem@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2018 Fox Sports' }
  
  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/c1458a27b9d12f341a4479bc85c3fb52a8f1ae90/FSVideoKit.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.framework'
  # s.resource_bundle         = { 'FSVideoKit' => 'Resources/**/*.{xcassets,otf}' }

  s.platform                = :ios
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  
  # internal dependencies
  s.dependency 'FSCore'     , '1.0.1'
  s.dependency 'FSPlayer'   , '1.0.1'

  # external dependencies
  s.dependency 'Auth0'      , '1.12.0'
  
end
