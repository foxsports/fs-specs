Pod::Spec.new do |s|
  s.name             = 'FSVideoKit'
  s.version          = '1.0.1'
  s.summary          = 'Fox Sports Video Kit'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Melad Barjel' => 'Melad.Barjel@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2022 Fox Sports' }
  
  s.source           = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/582444bdcc14cc95e8cfad2dc95c4a5d9f8ea6ad/Archives/FSVideoKit/FSVideoKit-1.0.1.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.framework'
  
  s.platform                = :ios
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '12.0'
  
  # internal dependencies
  s.dependency 'FSCore', '3.2.44'
  s.dependency 'FSPlayer', '1.4.34'

  # external dependencies
  s.dependency 'Auth0', '1.33.1'
  s.dependency 'YouboraAVPlayerAdapter', '6.6.6'
  s.dependency 'JWTDecode', '2.6.1'
  s.dependency 'Alamofire', '~> 5.4'
  
end
