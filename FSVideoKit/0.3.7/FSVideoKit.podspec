Pod::Spec.new do |s|
  s.name             = 'FSVideoKit'
  s.version          = '0.3.7'
  s.summary          = 'Fox Sports Video Kit'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Melad Barjel' => 'melad.barjel@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2018 Fox Sports' }
  
  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/bb2e41cffdcead8641bbfd33bce182611eae325d/Archives/FSVideoKit/FSVideoKit-0.3.7.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.framework'
  
  s.platform                = :ios
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  
  # internal dependencies
  s.dependency 'FSCore'     , '1.0.9'
  s.dependency 'FSPlayer'   , '1.0.9'

  # external dependencies
  s.dependency 'Auth0'      , '1.12.0'
  s.dependency 'YouboraAVPlayerAdapter', '6.3.3'
  s.dependency 'JWTDecode', '2.1.1'
  
end
