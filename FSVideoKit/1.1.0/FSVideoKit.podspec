Pod::Spec.new do |s|
  s.name             = 'FSVideoKit'
  s.version          = '1.1.0'
  s.summary          = 'Fox Sports Video Kit'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Alessandro Boron' => 'Alessandro.Boron@streamotion.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2022 Fox Sports' }
  
  s.source           = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/f0e3e968a85bda526956d67d48c12279f2a9a130/Archives/FSVideoKit/FSVideoKit-1.1.0.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.framework'
  
  s.platform                = :ios
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '12.0'
  
  # internal dependencies
  s.dependency 'FSCore', '3.2.45'
  s.dependency 'FSPlayer', '1.6.1'

  # external dependencies
  s.dependency 'Auth0', '1.39.0'
  s.dependency 'SimpleKeychain', '0.12.5'
  s.dependency 'YouboraAVPlayerAdapter', '6.6.6'
  s.dependency 'YouboraLib', '6.6.20'
  s.dependency 'JWTDecode', '2.6.1'
  s.dependency 'Alamofire', '~> 5.4'
  
end
