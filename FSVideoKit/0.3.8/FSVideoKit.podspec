Pod::Spec.new do |s|
  s.name             = 'FSVideoKit'
  s.version          = '0.3.8'
  s.summary          = 'Fox Sports Video Kit'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Melad Barjel' => 'melad.barjel@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2019 Fox Sports' }
  
  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/cbbbcaf6b103eae9d4bafe49a7d341dd0259a29a/Archives/FSVideoKit/FSVideoKit-0.3.8.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.framework'
  
  s.platform                = :ios
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  
  # internal dependencies
  s.dependency 'FSCore'     , '3.2.18'
  s.dependency 'FSPlayer'   , '1.3.19'

  # external dependencies
  s.dependency 'Auth0'      , '1.15.0'
  s.dependency 'YouboraAVPlayerAdapter', '6.3.3'
  s.dependency 'JWTDecode', '2.1.1'
  s.dependency 'Alamofire', '5.1'
  
end
