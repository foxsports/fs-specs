Pod::Spec.new do |s|
  s.name             = 'FSVideoKit'
  s.version          = '0.2.4'
  s.summary          = 'Fox Sports Video Kit'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Imran Raheem' => 'imran.raheem@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2018 Fox Sports' }
  
  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/f10d87ee83de76d97c8cc84ce2835132cf2a9135/FSVideoKit.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.framework'
  
  s.platform                = :ios
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  
  # internal dependencies
  s.dependency 'FSCore'     , '1.0.1'
  s.dependency 'FSPlayer'   , '1.0.1'

  # external dependencies
  s.dependency 'Auth0'      , '1.12.0'
  
end
