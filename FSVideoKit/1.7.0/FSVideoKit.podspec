Pod::Spec.new do |s|
  s.name             = 'FSVideoKit'
  s.version          = '1.7.0'
  s.summary          = 'Streamotion Video Kit'
  s.homepage         = 'https://streamotion.com.au'
  s.author           = { 'Rajesh Ramachandrakurup' => 'rajesh.ramachandrakurup@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2024 STREAMOTION PTY LTD' }
  
  s.source           = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/cf373810653fb8324c52c06efc330c4ace234264/Archives/FSVideoKit/FSVideoKit-1.7.0.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.xcframework'
  
  s.platform                = :ios
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '13.0'
  
  # internal dependencies
  s.dependency 'FSCore', '3.3.0'
  s.dependency 'FSPlayerKit', '1.7.0'

  # external dependencies
  s.dependency 'Auth0', '1.39.1'
  s.dependency 'SimpleKeychain', '0.12.5'
  s.dependency 'YouboraAVPlayerAdapter', '6.6.6'
  s.dependency 'YouboraLib', '6.6.20'
  s.dependency 'JWTDecode', '2.6.3'
  s.dependency 'Alamofire', '5.8.1'
  
end
