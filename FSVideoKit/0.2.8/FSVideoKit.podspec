Pod::Spec.new do |s|
  s.name             = 'FSVideoKit'
  s.version          = '0.2.8'
  s.summary          = 'Fox Sports Video Kit'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Imran Raheem' => 'imran.raheem@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2018 Fox Sports' }
  
  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/24b1377c57eef267055bcaecf14e945947f0ab8f/FSVideoKit-0.2.8.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.framework'
  
  s.platform                = :ios
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  
  # internal dependencies
  s.dependency 'FSCore'     , '1.0.1'
  s.dependency 'FSPlayer'   , '1.0.1'

  # external dependencies
  s.dependency 'Auth0'      , '1.12.0'
  s.dependency 'YouboraAVPlayerAdapter', '6.2.3'
  s.dependency 'JWTDecode', '2.1.1'
  
end
