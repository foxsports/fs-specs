Pod::Spec.new do |s|
  s.name             = 'FSVideoKit'
  s.version          = '0.3.3'
  s.summary          = 'Fox Sports Video Kit'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Imran Raheem' => 'imran.raheem@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2018 Fox Sports' }
  
  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/9353c295bee5cb5e40d666a8b4a6c28e55ecb5a8/Archives/FSVideoKit/FSVideoKit-0.3.3.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.framework'
  
  s.platform                = :ios
  s.swift_version           = '4.0'
  s.ios.deployment_target   = '10.0'
  
  # internal dependencies
  s.dependency 'FSCore'     , '1.0.5'
  s.dependency 'FSPlayer'   , '1.0.5'

  # external dependencies
  s.dependency 'Auth0'      , '1.12.0'
  s.dependency 'YouboraAVPlayerAdapter', '6.2.3'
  s.dependency 'JWTDecode', '2.1.1'
  
end
