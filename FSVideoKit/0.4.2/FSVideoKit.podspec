Pod::Spec.new do |s|
  s.name             = 'FSVideoKit'
  s.version          = '0.4.2'
  s.summary          = 'Fox Sports Video Kit'
  s.homepage         = 'http://foxsports.com.au'
  s.author           = { 'Melad Barjel' => 'melad.barjel@foxsports.com.au' }
  s.license          = { :type => 'Copyright', :text => 'Copyright 2019 Fox Sports' }
  
  s.source                  = { :http => 'https://bitbucket.org/foxsports/fs-specs/raw/b397b325cca54b209d320ee295e31786cbc9c98e/Archives/FSVideoKit/FSVideoKit-0.4.2.zip' }
  s.ios.vendored_frameworks = 'FSVideoKit.framework'
  
  s.platform                = :ios
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '10.0'
  
  # internal dependencies
  s.dependency 'FSCore'     , '3.2.39'
  s.dependency 'FSPlayer'   , '1.4.22'

  # external dependencies
  s.dependency 'Auth0'      , '1.31.0'
  s.dependency 'YouboraAVPlayerAdapter', '6.5.22'
  s.dependency 'JWTDecode', '2.6.0'
  s.dependency 'Alamofire', '~> 5.4'
  
end
